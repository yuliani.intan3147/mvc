<?php
class App {
    public function parse_url($uri=''){
        $uri = rtrim($uri,'/');
        $uri = filter_var($uri,FILTER_SANITIZE_URL);
        $params = explode('/',$uri);
        return $params;
    }

}
?>