<?php

    class Controller{
        public function model($model){
            require_once("path/to/{$model}.php");
            return new $model;
        }
        public function view ($new, $params=[]){
            require_once("path/to/new/{$new}.php");
        }
    }
?>