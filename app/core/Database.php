<?php
class Database {
    private $host = DB_HOST;
    private $user = DB_USER;
    private $pass = DB_PASS;
    private $name = name;
    private $pdoIns;
    private $queryIns;
    public function __construct() {
        $stringConn = "mysql:host={$this->host};dbname={$this->name}";
        $option = [PDO :: ATTR_ERRMODE,
                PDO :: ERRMODE_EXCEPTION ];
        try {
            $this->pdoIns = new PDO($stringConn, $this->user,$this->pass, $option);
        } catch(PDOException $e) {
            die($e->getMessage());
        }
    }
    public function query($query) {
        $this->queryIns = $this->pdoIns->prepare($query);
    }
    public function bind($param, $value, $type=null) {
        if(is_null($type)) {
            switch (true) {
                case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = PDO::PARAM_NULL;
                    break;
                default:
                    $type = PDO::PARAM_STR;
                    break;
            }
        }
        $this->queryIns->bindValue($param, $value, $type);
    }
    public function execute() {
        $this->queryIns->execute(); return true;
    }
    public function resultAll() {
        $this->execute();
        return $this->queryIns->fetchAll(PDO::FETCH_ASSOC);
    }
    public function resultSingle() {
        $this->execute();
        return $this->queryIns->fetch(PDO::FETCH_ASSOC);
    }
}  